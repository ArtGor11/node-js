const express = require('express');
const app = express();
const path = require("path");
const fs = require('fs');
const morgan = require('morgan');

app.use(express.json());
app.use(morgan('tiny'));

app.post('/api/files', (req, res) => {
  
  if (!req.body.filename) {
    return res.status(400).json({message: "Please specify 'filename' parameter"});
  }  
  
  if (!req.body.content) {
    return res.status(400).json({message: "Please specify 'content' parameter"});
  }

  let extention = path.extname(req.body.filename);

  const EXTENSIONS = ['.js', '.html', '.xml', '.json', '.yaml', '.log', '.txt'];

  if (!EXTENSIONS.includes(extention)) {
    return res.status(400).json({message: "Inappropriate extension name"}); 
  }

  fs.writeFile(path.join(__dirname, './createdFiles', req.body.filename), req.body.content, function (err) {

    if (err) {
      res.status(500).json({message: "Server error"});
    }

    res.status(200).json({message: "File created successfully"});

  });
});

app.get('/api/files', (req, res) => {
  
  fs.readdir('./createdFiles', (err, files) => {

    if (!files.length) {
      return res.status(400).json({ message: "Client error"});
    }

    if (err) {
        return res.status(500).json({message: "Server error"});
    }

    return res.status(200).json({"message": "Success", "files": files});

    });

});

app.get('/api/files/:fileName', (req, res) => {

  const fileName = req.params.fileName;

  route = path.join(__dirname, 'createdFiles/' + fileName);

  try {
    if (fs.existsSync(route)) {
      const fileStats = fs.statSync(route);
      const fileData = fs.readFileSync(route, { encoding: 'utf-8' });
      res.status(200).json({
        message: 'Success',
        filename: fileName,
        content: fileData,
        extension: path.extname(fileName).split('.')[1],
        uploadedDate: new Date(fileStats.ctime)
      });
    } else {
      res.status(400).json({
        message: "No file with '" + fileName + "' filename found"
      });
    }
  } catch (error) {
    res.status(500).json(error);
  }
});

if (!fs.existsSync('./createdFiles')) {
  fs.mkdirSync('./createdFiles');
}

app.listen(8080, () => {
  console.log('Server is running at port: 8080!')
});